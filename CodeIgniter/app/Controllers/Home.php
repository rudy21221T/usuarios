<?php namespace App\Controllers;
use CodeIgniter\Controller;
use App\Models\producto as UserModel;

class Home extends BaseController

{

	public function index()
	{
		$obj = new UserModel;
		$datos = $obj->get_producto();

		return view('home',['mostrar'=>$datos]);
	}


	//--------------------------------------------------------------------

}
