<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class login extends CI_Controller {
public function __construct(){
	parent::__construct();
	$this->load->model('loginM');
}
	
	public function index()
	{
		$data= array('title'=>'Login || Usuario');
		$this->load->view('template/header',$data);
		$this->load->view('loginV');
		$this->load->view('template/footer');
	}


public function mostrar()
	{
		$data= array('title'=>'Login || Usuario',
			'usuario'=>$this->loginM->get_usuario());
		$this->load->view('template/header',$data);
		$this->load->view('usuarioV');
		$this->load->view('template/footer');
	}
	public function iniciar(){
		$datos['usuario'] = $this->input->post('usuario');
		$datos['contrasenia'] = md5($this->input->post('clave'));

		$data = $this->loginM->validar($datos);

		
			redirect('login/mostrar','refresh');
		

	}

	public function ingresar(){
		$datos['nombre'] = $this->input->post('nombre');
		$datos['usuario'] = $this->input->post('usuario');
		$datos['contrasenia'] = md5($this->input->post('clave'));

		$data = $this->loginM->ingresar($datos);
			redirect('login/mostrar','refresh');
	}
}
