<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class loginM extends CI_Model {

	public function validar($datos){
		$this->db->where('usuario',$datos['usuario']);
		$this->db->where('contrasenia',$datos['contrasenia']);
		$exe = $this->db->get('ususario');
		if($this->db->affected_rows()>0){
			return $exe->row();
		}else{
			false;
		}
	}

	public function get_usuario(){
		$this->db->select('id_usuario,nombre,usuario,contrasenia');
		$this->db->from('ususario');
		$exe = $this->db->get();

		return $exe->result();
		
	}

	public function ingresar($datos){
		$this->db->set('nombre',$datos['nombre']);
		$this->db->set('usuario',$datos['usuario']);
		$this->db->set('contrasenia',$datos['contrasenia']);
		$this->db->insert('ususario');
		if($this->db->affected_rows()>0){
			return true;
		}else{
			false;
		}
	}
}
